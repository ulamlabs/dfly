######################################################################
#
# Copyright 2014 GotMyJobs, Inc.  All Rights Reserved.
#
######################################################################


import sys
import logging

log = logging.getLogger(__name__)

# Avoids logging things more than once
LOGGED_NAMESPACE = False
LOGGED_APPSTATS = False

# Limits the number of stack frames saved
appstats_MAX_STACK = 20

# Add ./lib to the path, so our dependencies can be found
if 'lib' not in sys.path:
    sys.path.insert(0, 'lib')
