import json
import logging
import datetime

from google.appengine.ext import ndb

import webapp2
from webapp2_extras import sessions
from pushjack import GCMClient
from webob import exc

GCM_APIKEY = 'AIzaSyDiyjnnhGKvyo2306gWFkhPKp32fQakVU4'
GCM_APIKEY = 'AIzaSyApaY_nKmGJkHxYVHIj_LuifU8kftKH1DE'
SENDER_ID = '128277843073'

client = GCMClient(api_key=GCM_APIKEY)


class Push(object):
    def __init__(self, user):
        self.user = user

    def send(self, *args, **kwargs):
        try:
            client.send(*args, **kwargs)
        except Exception as e:
            logging.error('??? %s', e)

    def msg_new(self, opponent, msg):
        data = {
            'notification': {
                'icon': 'icon',
                'title': "New message from {}".format(opponent.name),
                'body': msg[:50]
            },
            'userID': self.user.key.id(),
            'opponentID': opponent.key.id(),
            'type': 'chatHasChanged',
        }
        self.send(self.user.push_token, data)

    def msg_deleted(self, opponent, msgkey):
        data = {
            'userID': self.user.key.id(),
            'opponentID': opponent.key.id(),
            'index': msgkey,
            'type': 'chatMessageDeleted',
        }
        self.send(self.user.push_token, data)


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.isoformat()
        super(JSONEncoder, self).default(o)


class Chat(ndb.Model):
    @classmethod
    def get_key(cls, user_id, opponent_id):
        id_ = '_'.join(sorted([user_id, opponent_id]))
        key = ndb.Key(cls, id_)
        return key


class User(ndb.Model):
    name = ndb.StringProperty()
    push_token = ndb.StringProperty()
    push_type = ndb.StringProperty()


class Message(ndb.Model):
    text = ndb.TextProperty()
    author_key = ndb.KeyProperty(User)

    created_at = ndb.DateTimeProperty(auto_now_add=True)
    deleted = ndb.BooleanProperty(default=False)

    @classmethod
    def make(cls, user, opponent, text):
        chat_key = Chat.get_key(user.key.id(), opponent.key.id())
        return cls(
            author_key=user.key,
            text=text,
            parent=chat_key,
        )

    @property
    def chat_id(self):
        return self.key.parent().id()

    @property
    def author(self):
        return self.author_key.get()

    @property
    def opponent_key(self):
        id_ = (set(self.chat_id.split('_')) - {self.author_key.id()}).pop()
        return ndb.Key(User, id_)

    @property
    def opponent(self):
        return self.opponent_key.get()

    def to_dict(self, **kw):
        data = super(Message, self).to_dict(**kw)
        data.pop('author_key')
        data['key'] = self.key.urlsafe()
        data['userID'] = self.author_key.id()
        data['chat_id'] = self.chat_id
        data['opponentID'] = self.opponent_key.id()
        return data


class App(webapp2.WSGIApplication):
    pass


class View(webapp2.RequestHandler):
    auth = False

    def __init__(self, *args, **kwargs):
        super(View, self).__init__(*args, **kwargs)
        self.session_store = None

    @webapp2.cached_property
    def session(self):
        return self.session_store.get_session()

    @webapp2.cached_property
    def user(self):
        user_id = self.session['user_id']
        return ndb.Key(User, user_id).get()

    def abort(self, code, msg=None):
        cls = exc.status_map.get(code)
        response = cls()
        del response.headers['Content-Type']
        response.headers.add('Content-Type', 'applicaton/json')
        response.write(json.dumps({'message': msg or response.explanation}))
        raise response

    def dispatch(self):
        self.response.headers['Content-Type'] = 'application/json'
        self.session_store = sessions.get_store(request=self.request)

        if self.auth and not self.session:
            return self.abort(403, "Please POST /auth first")

        try:
            # Dispatch the request.
            return super(View, self).dispatch()
        except (KeyError, ValueError) as e:
            self.abort(400, repr(e))
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)

    def json_response(self, data=None, status_code=200):
        self.response.status_int = status_code
        if data:
            self.response.out.write(json.dumps(data, cls=JSONEncoder))

    def get_or_404(self, key):
        if isinstance(key, basestring):
            key = ndb.Key(urlsafe=key)
        obj = key.get()
        return obj or self.abort(404, "No such chat")


class MessagesView(View):
    auth = True

    def get(self):
        chat_id = self.request.params.get('chat_id')
        page_size = self.request.params.get('page_size', 10)
        cursor = self.request.params.get('cursor')
        cursor = cursor and ndb.Cursor(urlsafe=cursor)
        chat_key = ndb.Key(Chat, chat_id or 'randomstring')
        query = Message.query(ancestor=chat_key).filter(Message.deleted==False)
        query = query.order(-Message.created_at)
        objs, cursor, more = query.fetch_page(page_size=page_size, cursor=cursor)
        data = {
            'result': [o.to_dict() for o in objs],
            'cursor': cursor.urlsafe() if more else None,
        }
        self.json_response(data)

    def post(self):
        data = self.request.json
        text = data['text']
        opponent_id = data['opponentID']
        opponent = ndb.Key(User, opponent_id).get()
        if not opponent:
            self.abort(400, "No such oponent")
        if opponent.key == self.user.key:
            self.abort(400, "Can't send message to yourself")

        msg = Message.make(self.user, opponent, text)
        msg.put()

        Push(opponent).msg_new(self.user, text)
        self.json_response(msg.to_dict(), 201)


class MessageView(View):
    auth = True

    def get(self, key):
        message = self.get_or_404(key)
        return self.json_response(message.to_dict())

    def delete(self, key):
        message = self.get_or_404(key)
        if message.author_key != self.user.key:
            return self.abort(403, "You can't delete not your message")
        if message.deleted:
            return self.abort(404)
        opponent = message.opponent
        message.deleted = True
        message.put()
        Push(opponent).msg_deleted(self.user, message.key.urlsafe())
        return self.json_response(status_code=204)


class AuthView(View):
    def post(self):
        data = self.request.json
        user_id = data['userID']
        push_token = data['push_token']
        name = data['name']
        user = User(id=user_id, push_token=push_token, name=name)
        user.put()
        self.session['user_id'] = user.key.id()
        self.json_response({'status': 'ok'})


config = {
    'webapp2_extras.sessions': {
        'secret_key': 'aaa',
    }
}


app = App([
    webapp2.Route('/messages', handler=MessagesView),
    webapp2.Route('/messages/<key>', handler=MessageView),
    webapp2.Route('/auth', handler=AuthView),
], config=config)
