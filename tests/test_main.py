import main
import webtest


def test_app(ndb, mocker):
    client = mocker.patch('main.client')
    app = webtest.TestApp(main.app)

    resp = app.get('/messages', status=403)

    app.post_json('/auth', {'userID': 'user2', 'push_token': 't2', 'name': 'username2'})
    app.post_json('/auth', {'userID': 'user1', 'push_token': 't1', 'name': 'username1'})

    app.post_json('/messages', {'opponentID': 'user2', 'text': 'something1'})

    client.send.assert_called_once_with('t2', {
        'notification': {
            'body': u'something1',
            'title': 'New message from username1',
            'icon': 'icon'
        },
        'type': 'chatHasChanged',
        'opponentID': 'user1',
        'userID': 'user2'
    })
    client.send.reset_mock()

    app.post_json('/messages', {'opponentID': 'user2', 'text': 'something2'})

    client.send.assert_called_once_with('t2', {
        'notification': {
            'body': u'something2',
            'title': 'New message from username1',
            'icon': 'icon'
        },
        'type': 'chatHasChanged',
        'opponentID': 'user1',
        'userID': 'user2'
    })
    client.send.reset_mock()
    response = app.post_json('/messages', {'opponentID': 'user2', 'text': 'something'})
    assert response.status_int == 201

    chat_id = response.json['chat_id']
    response = app.get('/messages', params={'chat_id': chat_id})
    assert len(response.json['result']) == 3

    message_key = response.json['result'][0]['key']
    url = '/messages/{}'.format(message_key)
    response = app.get(url)
    assert response.status_int == 200
    client.send.reset_mock()
    response = app.delete(url)
    assert response.status_int == 204
    client.send.assert_called()
    client.send.assert_called_once_with('t2', {
        'type': 'chatMessageDeleted',
        'index': message_key,
        'opponentID': 'user1',
        'userID': 'user2',
    })
    client.send.reset_mock()

    app.delete(url, status=404)
    client.send.assert_not_called()
    response = app.get('/messages', params={'chat_id': chat_id})
    assert len(response.json['result']) == 2
