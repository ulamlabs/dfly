import os
import logging
import sys


def fix_sys_path():
    d = os.path.dirname

    _test_dir = d(os.path.abspath(__file__))
    _project_dir = d(_test_dir)
    _user_dir = os.path.abspath(os.path.expanduser('~'))

    _sdk_path = os.path.join(_user_dir, 'google-cloud-sdk/platform/google_appengine')

    sys.path.insert(0, _project_dir)
    sys.path.insert(0, _sdk_path)

    sys.path.insert(0, os.path.join(_project_dir, 'lib'))

    import dev_appserver
    dev_appserver.fix_sys_path()

fix_sys_path()

logging.basicConfig(
    level=logging.DEBUG,
    format='%(processName)s:%(asctime)s:%(name)s: %(levelname)s %(message)s'
)


os.environ["ENVIRONMENT_NAME"] = 'unittest'
